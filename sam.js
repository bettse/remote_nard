const util = require("util");
const pcsclite = require("@pokusew/pcsclite");
const debug = require("debug")("RemoteNard:SAM");

const sam_atr = Buffer.from("3b959680b1fe551fc7477261636513", "hex");

class SAM {
  constructor() {
    this.atr = Buffer.alloc(0);
    this.pcsc = pcsclite();
    this.pcsc.on("reader", (reader) => {
      if (reader.name.startsWith("Yubico")) {
        return;
      }
      // console.log("New reader detected", reader.name);

      reader.on("error", (err) => {
        console.log("Error(", reader.name, "):", err.message);
      });
      reader.on("status", this.readerStatusCB(reader));

      reader.on("end", () => {
        console.log("Reader", reader.name, "removed");
      });
    });

    this.pcsc.on("error", (err) => {
      console.log("PCSC error", err.message);
    });
  }

  connectCB(reader) {
    const max_expected_response = 0xFF;
    const sam = this;
    return function (err, protocol) {
      if (err) {
        console.log(err);
        return;
      }
      const _send = util.promisify(reader.transmit.bind(reader));

      sam.transmit = async (content) => {
        try {
          // make content a buffer if not already
          debug("->", content.toString("hex"));
          const reply = await _send(content, max_expected_response, protocol);
          debug("<-", reply.toString("hex"));
          return reply;
        } catch (e) {
          console.log("problem during transmit", { content, e });
          throw e;
        }
      };
    };
  }

  readerStatusCB(reader) {
    const sam = this;
    return function (status) {
      const { atr } = status;
      if (atr.toString("hex") !== sam_atr.toString("hex")) {
        return;
      }

      // console.log("Status(", reader.name, "):", status.state.toString(0x10));

      // check what has changed
      const changes = reader.state ^ status.state;

      if (!changes) {
        return;
      }

      if (
        changes & reader.SCARD_STATE_EMPTY &&
        status.state & reader.SCARD_STATE_EMPTY
      ) {
        debug("card removed");
        sam.atr = null;

        reader.disconnect(reader.SCARD_LEAVE_CARD, (err) => {
          if (err) {
            console.log(err);
            return;
          }

          console.log("Disconnected");
        });
      } else if (
        changes & reader.SCARD_STATE_PRESENT &&
        status.state & reader.SCARD_STATE_PRESENT
      ) {
        debug("card inserted");

        debug("ATR", atr.toString("hex"));
        sam.atr = atr;

        reader.connect(
          { share_mode: reader.SCARD_SHARE_EXCLUSIVE },
          sam.connectCB(reader)
        );
      }
    };
  }
}

module.exports = SAM;
