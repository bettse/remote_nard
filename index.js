require("dotenv").config();
const dgram = require("dgram");
const debug = require("debug")("RemoteNard");
const net = require("net");
const debounce = require("lodash.debounce");

const { CCID, CCIDMessage, CCIDSerial } = require("./ccid");
const SAM = require("./sam");

const HEX = 0x10;
const listen_port = 9000;
const listen_ip = "0.0.0.0";

// 0x02 = Slot 0 card removed
const removed = Buffer.from([
  CCID.MessageType.RDR_to_PC_NotifySlotChange,
  0x02,
]);
// 0x03 = Slot 0 card inserted
const inserted = Buffer.from([
  CCID.MessageType.RDR_to_PC_NotifySlotChange,
  0x03,
]);

const sam = new SAM();

// Change 1 byte length to 3 byte length
function fixAPDULength(payload) {
  if (payload[4] !== 0) {
    const newPayload = Buffer.alloc(payload.length + 2);
    payload.copy(newPayload, 0, 0, 4); // CLA, INS, P1, P2
    payload.copy(newPayload, 6, 4); // Lc
    return newPayload;
  }
  return payload;
}

const server = net.createServer();

let activeSocket = null;
const queue = [];

async function onData(data) {
  const socket = this;
  const { remoteAddress: address, remotePort: port } = socket;
  const packet = data;
  console.log(`---------------${Date()}----------------`);
  debug(`Packet: ${packet.toString("hex")}`);
  let response = null;

  const active =
    activeSocket &&
    activeSocket.remoteAddress === socket.remoteAddress &&
    activeSocket.remotePort === socket.remotePort;

  try {
    const ccid_serial = CCIDSerial.fromBuffer(packet);
    const msg = CCIDMessage.fromBuffer(ccid_serial.msg);

    if (active) {
      debouncedPop();
      switch (msg.bMessageType) {
        case CCID.MessageType.PC_to_RDR_GetSlotStatus:
          debug("PC_to_RDR_GetSlotStatus");
          response = new CCIDMessage(CCID.MessageType.RDR_to_PC_SlotStatus);
          if (sam.atr.length > 0) {
            response.bStatus = CCID.SlotStatus.PRESENT;
          } else {
            response.bStatus = CCID.SlotStatus.ABSENT;
          }
          break;
        case CCID.MessageType.PC_to_RDR_IccPowerOn:
          debug("PC_to_RDR_IccPowerOn");
          response = new CCIDMessage(
            CCID.MessageType.RDR_to_PC_DataBlock,
            sam.atr
          );
          break;
        case CCID.MessageType.PC_to_RDR_SetParameters:
          debug("PC_to_RDR_SetParameters");
          response = new CCIDMessage(CCID.MessageType.RDR_to_PC_Parameters);
          break;
        case CCID.MessageType.PC_to_RDR_XfrBlock:
          // Special case for IFSD
          if (msg.dwLength === 5 && msg.payload.toString('hex') === '00c101fe3e') {
            debug("PC_to_RDR_XfrBlock IFSD");
            response = new CCIDMessage(
              CCID.MessageType.RDR_to_PC_DataBlock,
              Buffer.from([0x00, 0xE1, 0x01, 0xfe, 0x3e])
            );
            break;
          }
          debug("PC_to_RDR_XfrBlock");
          // Slice to remove the T=1 frame
          const apdu = fixAPDULength(msg.payload.slice(3, -1));
          const sam_reply = await sam.transmit(apdu);
          response = new CCIDMessage(
            CCID.MessageType.RDR_to_PC_DataBlock,
            sam_reply
          );
          break;
        default:
          console.log(
            "Unknown message type 0x",
            msg.bMessageType.toString(HEX)
          );
      }
    } else {
      switch (msg.bMessageType) {
        case CCID.MessageType.PC_to_RDR_GetSlotStatus:
          debug("PC_to_RDR_GetSlotStatus");
          response = new CCIDMessage(CCID.MessageType.RDR_to_PC_SlotStatus);
          response.bStatus = CCID.SlotStatus.ABSENT;
          break;
        default:
          // TOOD: What is the error message when you send data to a SAM that is not present?
          console.log(
            "Unknown message type 0x",
            msg.bMessageType.toString(HEX)
          );
      }
    }
  } catch (e) {
    // GET request
    if (packet.slice(0, 3).toString("hex") === "474554") {
      // console.log("GET request, closing socket");
      activeSocket.destroy();
      return;
    }
    console.log(e);
  }

  if (response) {
    const serial = new CCIDSerial(
      CCIDSerial.SYNC,
      CCIDSerial.CTRL,
      response.toBuffer()
    );
    const reply = serial.toBuffer();

    debug(`reply ${reply.toString("hex")}`);
    socket.write(reply, function (error) {
      if (error) {
        console.log("Error sending message", error);
      }
    });
  }
}

const debouncedPop = debounce(nextSocket, 60 * 1000, {
  maxWait: 10 * 60 * 1000,
});
function nextSocket() {
  console.log(
    "nextSocket: Queue length",
    queue.length,
    activeSocket === null ? "[no" : "[",
    "active socket]"
  );

  if (queue.length > 0) {
    if (activeSocket !== null) {
      debug("fake removed", removed);
      activeSocket.end(removed, function (error) {
        if (error) {
          console.log("Error sending message", error);
          return;
        }
      });
    }

    activeSocket = queue.pop();

    debug("fake inserted", inserted);
    activeSocket.write(inserted, function (error) {
      if (error) {
        console.log("Error sending message", error);
      }
    });
    debouncedPop();
  }
}

server.on("connection", function (socket) {
  console.log("CONNECTED: " + socket.remoteAddress + ":" + socket.remotePort);
  queue.push(socket);
  if (activeSocket === null) {
    nextSocket();
  } else {
    debouncedPop();
  }

  socket.on("close", function (data) {
    const active =
      activeSocket &&
      activeSocket.remoteAddress === socket.remoteAddress &&
      activeSocket.remotePort === socket.remotePort;
    if (active) {
      activeSocket = null;
    }

    let index = queue.findIndex(function (o) {
      return (
        o.remoteAddress === socket.remoteAddress &&
        o.remotePort === socket.remotePort
      );
    });
    if (index !== -1) queue.splice(index, 1);
    console.log("CLOSED: " + socket.remoteAddress + " " + socket.remotePort);
  });

  socket.on("error", (error) => {
    console.log("error", error);
  });

  socket.on("data", onData.bind(socket));
});

server.listen(listen_port, listen_ip, function (error) {
  if (error) {
    console.log("error", error);
  }
  console.log("Server listening on: " + listen_ip + ":" + listen_port);
});
