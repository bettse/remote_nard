const CCID = {
  MessageType: {
    RDR_to_PC_NotifySlotChange: 0x50,
    PC_to_RDR_SetParameters: 0x61,
    PC_to_RDR_IccPowerOn: 0x62,
    PC_to_RDR_IccPowerOff: 0x63,
    PC_to_RDR_GetSlotStatus: 0x65,
    PC_to_RDR_Escape: 0x6b,
    PC_to_RDR_GetParameters: 0x6c,
    PC_to_RDR_ResetParameters: 0x6d,
    PC_to_RDR_XfrBlock: 0x6f,
    RDR_to_PC_DataBlock: 0x80,
    RDR_to_PC_SlotStatus: 0x81,
    RDR_to_PC_Parameters: 0x82,
    RDR_to_PC_Escape: 0x83,
    RDR_to_PC_DataRateAndClockFrequency: 0x84,
  },
  SlotStatus: {
    ABSENT: 0x02,
    PRESENT: 0x00,
  },
};

function hexDump(buffer) {
  let str = "";
  for (let i = 0; i < buffer.length; i++) {
    if (buffer[i] >= 0x20 && buffer[i] < 0x7f) {
      str += String.fromCharCode(buffer[i]);
    } else {
      str += ".";
    }
  }
  return str;
}

class CCIDSerial {
  static SYNC = 0x03;
  static CTRL = 0x06;
  static NAK = 0x15;

  static LRC(buffer) {
    let lrc = 0;
    for (let i = 0; i < buffer.length; i++) {
      lrc = (buffer[i] ^ lrc) & 0xff;
    }
    return lrc;
  }

  static fromBuffer(buffer) {
    if (buffer.length < 3) {
      throw new Error("Buffer too short");
    }
    let msg = new CCIDSerial();
    msg.sync = buffer.readUInt8(0);
    msg.ctrl = buffer.readUInt8(1);
    if (msg.sync !== CCIDSerial.SYNC || msg.ctrl !== CCIDSerial.CTRL) {
      if (buffer.slice(0, 3).toString("hex") !== "474554") {
        console.log("Hexdump: ", hexDump(buffer));
      }
      throw new Error("Invalid sync or control byte");
    }
    msg.msg = buffer.slice(2, buffer.length - 1);
    msg.lrc = buffer.readUInt8(buffer.length - 1);
    const expected = CCIDSerial.LRC(buffer.slice(0, buffer.length - 1));
    if (msg.lrc !== expected) {
      throw new Error("LRC error");
    }
    return msg;
  }

  toBuffer() {
    let buffer = Buffer.alloc(2 + this.msg.length + 1);
    buffer.writeUInt8(this.sync, 0);
    buffer.writeUInt8(this.ctrl, 1);
    this.msg.copy(buffer, 2);
    this.lrc = CCIDSerial.LRC(buffer.slice(0, -1));
    buffer.writeUInt8(this.lrc, buffer.length - 1);
    return buffer;
  }

  constructor(
    sync = CCIDSerial.SYNC,
    ctrl = CCIDSerial.CTRL,
    msg = Buffer.alloc(0)
  ) {
    this.sync = sync;
    this.ctrl = ctrl;
    this.msg = msg;
  }
}

class CCIDMessage {
  static sequence = 0;
  static slot = 0;

  static getSequence() {
    return CCIDMessage.sequence++;
  }

  static fromBuffer(buffer) {
    let msg = new CCIDMessage();
    msg.bMessageType = buffer.readUInt8(0);
    msg.dwLength = buffer.readUInt32LE(1);
    msg.bSlot = buffer.readUInt8(5);
    msg.bSeq = buffer.readUInt8(6);
    msg.bStatus = buffer.readUInt8(7);
    msg.bError = buffer.readUInt8(8);
    if (msg.dwLength > 0) {
      msg.payload = buffer.slice(10, 10 + msg.dwLength);
    } else {
      msg.payload = Buffer.alloc(0);
    }
    return msg;
  }

  constructor(msgType = 0, payload = Buffer.alloc(0)) {
    this.bMessageType = msgType;
    this.dwLength = payload.length;
    this.bSlot = CCIDMessage.slot;
    this.bSeq = CCIDMessage.getSequence();
    this.bStatus = 0;
    this.bError = 0;
    // bClockStatus
    this.payload = payload;
  }

  toBuffer() {
    let buffer = Buffer.alloc(10 + this.dwLength);
    buffer.writeUInt8(this.bMessageType, 0);
    buffer.writeUInt32LE(this.dwLength, 1);
    buffer.writeUInt8(this.bSlot, 5);
    buffer.writeUInt8(this.bSeq, 6);
    buffer.writeUInt8(this.bStatus, 7);
    buffer.writeUInt8(this.bError, 8);
    this.payload.copy(buffer, 10);
    return buffer;
  }
}

module.exports = {
  CCID,
  CCIDMessage,
  CCIDSerial,
};
