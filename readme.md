# Remote NARD

~~Allow Seader to use a remote SAM via the Flipper Wifi Dev board and esp-at firmware.~~

Due to T=1 support being added to Seader, this no longer works until someone writes a T=1 layer for this.

## Setup Flipper Wifi Dev board

1. Install the custom `esp-at` firmware (`esp-at.bin.zip`) on the Flipper Wifi Dev board (I use https://esp.huhn.me/). (Offset 0)
2. Connect the Flipper Wifi Dev board to your flipper.
3. Launch USB-UART in the GPIO app
4. Connect using a serial terminal (I use https://serial.huhn.me/).  Some users have reported issues with Putty
(optional) press "RESET" button and look for boot logs to confirm the firmware is loaded correctly.
5. Switch to pins 15,16 in the USB-UART app.
6. Run the following commands, You should see "OK" after each, except "AT+CWJAP" which will take a few seconds to connect to the AP. Replace `<AP NAME>`, `<AP PASSWORD>`, `<HOST>`, and `<port>` with your values.

```bash
AT+SYSSTORE=1
AT+CWINIT=1
AT+CWMODE=3
AT+CWJAP="<AP NAME>","<AP PASSWORD>"

AT+SAVETRANSLINK=1,"<HOST>",<port>,"TCP",1000
```

7. Push "RESET" button on the Flipper Wifi Dev board to restart it into Data Mode.
8. Launch Seader.


**NOTE**: If you have issues and need to use AT commands again, send `+++` (no newline) to the serial terminal to enter AT command mode (https://docs.espressif.com/projects/esp-at/en/latest/esp32/AT_Command_Set/TCP-IP_AT_Commands.html#cmd-plus).  If you try to do this using a web serial tool, be sure to switch to "none" for the line ending before sending "+++".  After sending "+++", switch back to "\r\n" for line ending and send a blank line to get "ERROR" back.

## Prerequisites

- libpcsclite-dev libccid opensc opensc-pkcs11 pcscd
- Node.js

## Running node service

1. `yarn install`
2. `node index.js` (optionally `DEBUG=RemoteNard,RemoteNard:* node index.js` to see debug logs)
